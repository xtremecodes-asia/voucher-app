﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;

namespace VoucherApp.Helpers
{
    public class FileUploadHelper
    {
        const string AZURE_STORAGE_ACCOUNT_NAME = "azurebali1234";
        const string AZURE_STORAGE_ACCOUNT_KEY = "jIDjm7gnkaR06nSsJX6mVyPbe4oKyJ0C3dmPGz6qE8dBTKC6W7N3tOAOpC4OHXqHoTilX4PDoCnm6XppYCCU3g==";
        const string CONTAINER_NAME = "bali1234";
        const string AZURE_STORAGE_ENDPOINT = "https://azurebali1234.blob.core.windows.net/{0}/{1}";

        public static string UploadFile(Stream file)
        {
            string fileName = Guid.NewGuid().ToString() + ".jpg";
            
            try
            {
                StorageCredentials credentials = new StorageCredentials(AZURE_STORAGE_ACCOUNT_NAME, AZURE_STORAGE_ACCOUNT_KEY);
                CloudStorageAccount storageAccount = new CloudStorageAccount(credentials, true);

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference(CONTAINER_NAME);
                try
                {
                    container.CreateIfNotExists();
                }
                catch (StorageException)
                {
                    throw;
                }

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
                blockBlob.UploadFromStream(file);


                return String.Format(AZURE_STORAGE_ENDPOINT, CONTAINER_NAME, fileName);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}