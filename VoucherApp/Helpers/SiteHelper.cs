﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoucherApp.Helpers
{
    public class SiteHelper
    {
        public static string GetBaseUrl()
        {
            string baseUrl = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority +
                               HttpContext.Current.Request.ApplicationPath.TrimEnd('/');
            return baseUrl;
        }

        public static string GetCurrentUrl()
        {
            return HttpContext.Current.Request.Url.AbsoluteUri;
        }
    }
}