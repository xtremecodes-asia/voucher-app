﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VoucherApp.Helpers;

namespace VoucherApp.Models
{
    public class Registrant
    {
        [Required(ErrorMessage = "Nama lengkap harus diisi")]
        [Display(Name = "Nama lengkap sesuai kartu identitas diri")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nomor telepon harus diisi")]
        [Phone]
        [Display(Name = "Nomor telepon")]
        public string Number { get; set; }

        [Required(ErrorMessage = "Alamat email harus diisi")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Alamat email")]
        public string Email { get; set; }

       // [Required(ErrorMessage = "Screenshot MixRadio harus dipilih")]
        [DataType(DataType.Upload)]
       // [ValidateFile]
        [Display(Name = "Upload screenshot")]
        public HttpPostedFileBase ImageUpload { get; set; }

        
        public long Id { get; set; }
        public string ErrorMessage { get; set; }
    }
}