﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoucherApp.Helpers;
using VoucherApp.Models;

namespace VoucherApp.Controllers
{
    public class HomeController : Controller
    {
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult Index(bool? Error)
        {
            var vm = new RegistrantsViewModel();
            if (Error.HasValue)
            {
                vm.ErrorMessage = "Failed, please try again in a few moment.";
                return View(vm);
            }
            else
                return View();
            
        }

        [HttpPost]
        public ActionResult Index(Registrant model)
        {
            //valid
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var photoUrl = "";
            photoUrl = FileUploadHelper.UploadFile(model.ImageUpload.InputStream);

            VoucherApp.DAO.VoucherRegistrantRepository repository = new DAO.VoucherRegistrantRepository();
            var result = repository.Save(model.Name, model.Number, model.Email, photoUrl);
            if (result == null)
                return View(model);
            else
                return RedirectToAction("ThankYou", "Home");
        }

        public ActionResult ThankYou()
        {
            return View();
        }
    }
}