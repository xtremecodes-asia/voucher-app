﻿using VoucherApp.Helpers;
using VoucherApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VoucherApp.Controllers
{
    public class RegistrantController : Controller
    {
        // GET: /Registrant/Index
        public ActionResult Index(string error)
        {
            var vm = new RegistrantsViewModel();
            vm.Registrants = new ObservableCollection<VoucherRegistrant>();

            try 
            {
                vm.Registrants.Add(new VoucherRegistrant() { Name = "Puja Pramudya", Id = 1, Email = "puja@radyalabs.com", Mobile = "085265204094", PhotoUpload = "https://cms.dailysocial.id/wp-content/uploads/2016/03/laundry-413688_1280.jpg" });
                vm.Registrants.Add(new VoucherRegistrant() { Name = "Puja Pramudya", Id = 1, Email = "puja@radyalabs.com", Mobile = "085265204094", PhotoUpload = "https://cms.dailysocial.id/wp-content/uploads/2016/03/laundry-413688_1280.jpg" });
                vm.Registrants.Add(new VoucherRegistrant() { Name = "Puja Pramudya", Id = 1, Email = "puja@radyalabs.com", Mobile = "085265204094", PhotoUpload = "https://cms.dailysocial.id/wp-content/uploads/2016/03/laundry-413688_1280.jpg" });
                vm.Registrants.Add(new VoucherRegistrant() { Name = "Puja Pramudya", Id = 1, Email = "puja@radyalabs.com", Mobile = "085265204094", PhotoUpload = "https://cms.dailysocial.id/wp-content/uploads/2016/03/laundry-413688_1280.jpg" });

                //using (var db = new VoucherDataContext())
                //{
                //    vm.Registrants = new ObservableCollection<VoucherRegistrant>(
                //        db.VoucherRegistrants.ToList());
                //}
            } 
            catch(Exception) { }

            if (!string.IsNullOrWhiteSpace(error))
                vm.ErrorMessage = error;

            return View(vm);
        }

    
    }
}