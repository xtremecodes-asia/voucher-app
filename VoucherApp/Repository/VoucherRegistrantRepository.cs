﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VoucherApp.Models;

namespace VoucherApp.DAO
{
    public class VoucherRegistrantRepository
    {
        private readonly VoucherDataContext db;

        public VoucherRegistrantRepository()
        {
            db = new VoucherDataContext();
        }
        

        public VoucherRegistrant Save(string name, string phone, string email, string photoUrl)
        {
                try
                {
                    var registrant = new VoucherRegistrant()
                    {
                        Name = name,
                        Email = email,
                        Mobile = phone,
                        PhotoUpload = photoUrl,
                        VoucherID = null
                    };
                    db.VoucherRegistrants.InsertOnSubmit(registrant);
                    db.SubmitChanges();

                    return registrant;
                }
                catch 
                { 
                    return null; 
                }
            
        }

        public bool Edit(VoucherRegistrant registrant)
        {
                try
                {
                    var registrantData = db.VoucherRegistrants.FirstOrDefault(item => item.Id == registrant.Id);
                    registrantData.Name = registrant.Name;
                    registrantData.Email = registrant.Email;
                    registrantData.Mobile = registrant.Mobile;
                    registrantData.PhotoUpload = registrant.PhotoUpload;
                    registrantData.VoucherID = registrant.VoucherID;

                    db.SubmitChanges();
                    return true;
                }
                catch 
                { 
                    return false; 
                }
            
        }

        public bool Delete(long id)
        {
               try
               {
                    db.VoucherRegistrants.DeleteOnSubmit(db.VoucherRegistrants.FirstOrDefault(item => item.Id == id));
                    db.SubmitChanges();
                    return true;
               }
               catch 
               { 
                    return false; 
               }
        }

    }
}